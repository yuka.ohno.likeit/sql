﻿CREATE TABLE item(
  item_id SERIAL,
  item_name varchar(256),
  item_price int NOT NULL,
  category_id int
);


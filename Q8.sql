﻿SELECT item_id, item_name, item_price, category_name
    FROM item_category
    JOIN item
    ON item_category.category_id = item.category_id;

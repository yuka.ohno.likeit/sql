﻿SELECT category_name, SUM(item_price) AS total_price
    FROM item_category
    JOIN item
    ON item_category.category_id = item.category_id
    GROUP BY category_name
    ORDER BY SUM(item_price) DESC;
    

